# devops-netology
add something

========

В файле .gitignore будут игнорироваться файлы из папки .idea и файлы из папки vendor.

# Local .terraform directories - будут проигнорированы все файлы .terraform в любой из подкатегорий проекта 
**/.terraform/* 

# .tfstate files - исключаются файлы с расширением .tfstate и файлы, где встречается .tfstate.
*.tfstate
*.tfstate.*

# Crash log files - исключается файл crash.log
crash.log

# Exclude all .tfvars files, which are likely to contain sentitive data, such as
# password, private keys, and other secrets. These should not be part of version 
# control as they are data points which are potentially sensitive and subject 
# to change depending on the environment.
#
*.tfvars - исключаются файлы с расширением .tfvars

# Ignore override files as they are usually used to override resources locally and so
# are not checked in - исключаются файлы override.tf, override.tf.json etc.
override.tf
override.tf.json
*_override.tf
*_override.tf.json

# Include override files you do wish to add to version control using negated pattern
#
# !example_override.tf - не исключать файл example_override.tf

# Include tfplan files to ignore the plan output of command: terraform plan -out=tfplan
# example: *tfplan*

# Ignore CLI configuration files
.terraformrc - исключить
terraform.rc - исключить
new string
here is new string
Hello World!
GIT PUSH VIA PyCharm!
